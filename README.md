# menagerie

a collection of random python scripts and other things i've made

## Table of Contents

Click the title of any section to return to the table of contents.

1. [pypasswd](#pypasswd)
2. [eighty](#eighty)
3. [adrenoma](#adrenoma)
4. [renamer](#renamer)

## [pypasswd](#table-of-contents)

Originally written in Pythonista for iOS, this is yet another Python password
generator.

You get to chose length (8-64 characters), presence of capitals, symbols and
numbers.

In the end, the new password is printed to the console.

The \_commented version includes comments and prints the workings of the
password generator as it's working.

## [eighty](#table-of-contents)

Two Python scripts to convert text in a document to a maximum character width of
80 per line. They are designed for use in iOS via Pythonista or Editorial.

The Pythonista version takes the contents of the system clipboard, performs
the line width adjustment and re-sets the clipboard with the re-formatted text.
This means other text editing applications can make use of the functionality by
running it via Pythonista in the share sheet.

The Editorial version takes the content of whatever is open in Editorial, re-
formats it and replaces it. This means no text need be selected nor does
clipboard need to be set. I plan to update it so that if text is already
selected, this will be re-formatted and if not, the whole document will be. This
flexibility might be useful. Note, the `editor` module used to grab the text in
the editor is also present in Pythonista, but Pythonista already alerts the user to
excessive line widths so it isn’t really needed.

The Drafts version sits (as a .py file) within Pythonista and is called by running
a url action from Drafts. This is detailed in the comment at the top of the file.
It sends the whole contents of the current Draft to Pythonista, re-formats it and
re-opens the Draft with the text changed to the re-formatted text.

## [adrenoma](#table-of-contents)

 A basic Python script for calculating washout characteristics of adrenal lesions 

- It was the first Python script I ever wrote
- It calculates washout characteristics for adrenal lesions on computed tomography
  (CT,) tells you what the value is and tells you if that means the lesion is benign or not
- If you google, you'll find many of these calculators online
- But hey, it was my first program and we've all got to start somewhere!

Assuming you are in the directory of the "adrenoma.py" file, try:

`python3 adrenoma.py`

or:

`./adrenoma.py`

although you might have to make the file executable for that to work, which can be
done like this:

`sudo chmod +x adrenoma.py`

## [renamer](#table-of-contents)

A basic Bash script for taking a bunch of files and renaming them sequentially.

Originally conceived for batch photo album renaming

- All files in a directory are renamed
- First, the user is asked for the file extension (this way, eg. jpg & jpeg can
  be merged into one or the other after rename)
- The user is then asked for a prefix (eg. 'Honeymoon')
- Current file order is retained
- The new files are renamed 'prefix-0001.extension' and so on
