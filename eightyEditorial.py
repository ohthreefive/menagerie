# import functions from modules
from textwrap import wrap
from editor import get_text, set_selection, insert_text

# set variable to text in editor using get_text() function
inputText = get_text()

# convert this string to a list, using new lines to split string
inputList = inputText.splitlines()

# declare empty list which the following loop will fill
outputList = []

# loop through list (which contains get_text)
for i in inputList:
# if line was blank, add NEWLINE to outputList
# (if i dont do this, these empty lines will be lost by the wrap() function)
	if i == '':
		outputList.append('NEWLINE')
# otherwise use wrap() function to limit line width to 80 characters
# (wrap() outputs to a list which ive called 'shortened')
	else:
		shortened = wrap(i, width = 80)
# iterate through the shortened list, adding each entry of that list to
# outputList
# if i just added 'shortened' to 'outputList', i would have lists within a list
# i want one list
		for j in shortened:
			outputList.append(j)

# take the outputList and make it back in to a string, adding a new line
# between each line item
outputText = "\n".join(outputList)
# replace the NEWLINE placeholder with nothing, restoring the empty lines
outputText = outputText.replace("NEWLINE","")

# select all text in editor by selecting from 0 characters to the length (in
# characters) of the contents of the editor
set_selection(0,len(get_text()))

# replace selected text with newly edited text
insert_text(outputText)