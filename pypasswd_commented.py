# import two methods from the 'random' module
from random import choice as c, shuffle

# import 'sleep' method from 'time' module to slow programme down in this
# de-bugger
from time import sleep as s

# declare first function of the programme; it will ask for a password length
def getNumber():

# start an infinite loop, which will only be terminated by 'break' or declaring
# 'False'
	while True:

# the while loop runs a "try-except" block, meaning if the user enters anything
# other than the desired input (a number) the loop re-starts rather than the
# programme crashing
		try:

# the user is asked for a number between 8 & 64
# as 'input' returns a 'string', immediately convert the input to an integer by
# wrapping it in 'int()'
# the value is stored in the variable, 'length'
			length = int(input("What length of password would you"
						+ "like?" + "\n" + "(8-64 characters please)" + "\n" +
						"\n"))

# if any number is typed, it will be successfully converted to an integer above
# and the 'While' loop will be broken, moving the programme onwards
			break

# if anything other than a number was entered, above, the programme's attempt
# to convert it to an integer will cause a ValueError and this 'except' clause
# will catch that error and re-start the loop (ie. ask for a number again)
		except ValueError:

# tell the user what they did wrong before re-starting the loop
			print("\n" + "That's not a number. You'll need to start"
						+ "again." + "\n")

# this 'if' block can only be reached if a number was entered above.
# it enforces the programme's 8 to 64 character password limit, without giving
# the user an opportunity to re-choose a password length.
	if length < 8:
		print("\n" + "Too short, your password length has been" +
				" increased to 8" + "\n")
		length = 8
	elif length > 64:
		print("\n" + "Too long, your password length has been decreased"
				+ " to 64" + "\n")
		length = 64

# This print statement is for de-bugging only and is not in the normal
# programme
	print("\n" +"You have chosen a password", length, "characters long.")

# the variable 'length' is returned by the programme, either being that entered
# by the user or altered by the 'if-else' block
	return length

# declare second function of the programme; it will ask for password components
def getComponents():

# first declare the variable 'components' which contains a list, initially only
# containing '0'
# the values in the list will be used to represent the following password
# components; this will be important later in the programme:
# 0 : lowercase letters
# 1 : uppercase letters
# 2 : numbers
# 3 : symbols
# could i have implemented a dictionary instead of two lists?
	components = [0]

# start an infinite loop, again, asking the user whether they want uppercase
# letters in the password
	while True:

# ask the user whether they want uppercase letters in their password and store
# it in the variable 'upper'; prompting the user to enter 'y' or 'n'.
# convert whatever the user enters to lowercase using '.lower()' in case they
# enter uppercase or have caps lock on!
		upper = input("\n" + "Include upper case/capital letters?" +
						"\n" + "(y/n)" + "\n" + "\n").lower()

# check if the contents of variable 'upper' are in the list; again a simple
# user error is captured here, in case the user entered 'yes' instead of the
# requested 'y'.
# assuming 'y' or 'yes' has been entered, a '1' will be appended to the above
# list 'components' and the infinite loop will be terminated by 'break'
		if upper in ["y", "yes"]:
			components.append(1)
			break

# if 'n' or 'no' was entered, the infinite loop will be terminated without
# adding anything to the 'components' list
		elif upper in ["n", "no"]:
			break

# if anything other than 'y', 'yes', 'n' or 'no' was entered, the loop
# continues by re-requesting 'y/n'

# the above 'While' loop is repeated, first for whether the password should
# contain numbers, then for symbols
	while True:
		numerals = input("\n" + "Include numbers?" + "\n" + "(y/n)" +
						"\n" + "\n").lower()
		if numerals in ["y", "yes"]:
			components.append(2)
			break
		elif numerals in ["n", "no"]:
			break

	while True:
		symbols = input("\n" + "Include symbols?" + "\n" + "(y/n)" +
						"\n" + "\n").lower()
		if symbols in ["y", "yes"]:
			components.append(3)
			break
		elif symbols in ["n", "no"]:
			break

# This print statement is for de-bugging only and is not in the normal
# programme
# It shows the user which password components they have chosen
	for i in components:
		if i == 0:
			print("\n" + "Your password will contain lowercase"
								+ " characters.")
		elif i == 1:
			print("\n" + "Your password will contain uppercase"
								+ " characters.")
		elif i == 2:
			print("\n" + "Your password will contain numeric"
								+ " characters.")
		elif i == 3:
			print("\n" + "Your password will contain symbolic"
								+ " characters.")
		s(1)

# after these three loops have been terminated, the variable 'components' will
# be returned, containing a list between [0] and [0,1,2,3] depending on user
# input within this function
# im sure these three 'While' loops could be condensed in to one but this way
# works for me
	return components

# the programme's third function takes the variables generated by the first two
# functions and combines them to make a password
def makePass(userLength, pasComponents):

# first declare the variable 'password' as an empty string
	password = ""

# next declare the variable 'chars'(characters), which contains a list of
# lowercase, uppercase, numeric and symbolic characters which will be used to
# generate a password
	chars = ["abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
	"0123456789", "-/:;()£&@.,?![]{}#%^*+=_\|~<>€$"]

# as the password must contain at least one of each user selected component,
# first loop through the list of components with a 'for' loop
	for i in pasComponents:

# within that for loop, add a randomly selected (using the 'c()' method
# imported from the 'random' module at the beginning) character from the
# string contained at position 'i' in the list 'chars'
# if 'pasComponents' was [0,2] then a random lowercase then numeric
# character would have been added to the 'password' variable
# THE CODE BELOW IS ALTERED COMPARED TO THE ORIGINAL PROGRAMME FOR EASIER
# DE-BUGGING - THE print() 'if' LOOP IS THE MAIN CHANGE
		x = c(chars[i])
		password += x
		if i == 0:
			print("\n" + "First mandatory component is: lowercase."
								+ "\n" + "Random character is: " + x + "\n" +
								"Password currently is: " + password)
		elif i == 1:
			print("\n" + "Next mandatory component is: uppercase."
								+ "\n" + "Random character is: " + x + "\n" +
								"Password currently is: " + password)
		elif i == 2:
			print("\n" + "Next mandatory component is: numerical."
								+ "\n" + "Random character is: " + x + "\n"
								+ "Password currently is: " + password)
		elif i == 3:
			print("\n" + "Next mandatory component is: a symbol." +
								"\n" + "Random character is: " + x + "\n" +
								"Password currently is: " + password)
		s(1)

# as we have already started adding characters to the password, the programme
# renoves that many characters from the length of password the user originally
# asked for
# if 'pasComponents' was [0,2] then a random len(pasComponents) would be 2 and
# the line below re-declare the variable userLength as 2 less than it was
# print() STATEMENT ADDED COMPARED TO USUAL PROGRAMME
	userLength -= len(pasComponents)
	print("\n" + "The remaining", userLength, "characters of your password"
		+ " will now contain a random selection of the chosen components")
	s(1)

# to fill in the rest of 'password' randomly, first start a 'for' loop which
# will repeat as many times as the variable 'userLength' is long; that is what
# the 'range()' does here
	for i in range(0,(userLength)):

# every time the loop repeats, pick a random (using 'c()') component from the
# 'pasComponents' list and assign it the the variable 'j'
# THE CODE BELOW IS ALTERED COMPARED TO THE ORIGINAL PROGRAMME FOR EASIER
# DE-BUGGING - THE print() 'if' LOOP IS THE MAIN CHANGE
		j = c(pasComponents)
		if j == 0:
			print("\n" + "Random component is: lowercase.")
		elif j == 1:
			print("\n" + "Random component is: uppercase.")
		elif j == 2:
			print("\n" + "Random component is: numerical.")
		elif j == 3:
			print("\n" + "Random component is: a symbol.")
		s(1)

# then pick a random character from the list 'chars' at position (or index) 'j'
# if 'pasComponents' was [0,2] and 'c(pasComponents)' has assigned '2' to 'j'
# then below would pick a random numeral and add it to the 'password' variable
# print() STATEMENT ADDED COMPARED TO USUAL PROGRAMME
		x = c(chars[j])
		password += x
		print("Random character is: " + x)
		s(1)
		print("\n" + "Password now is: " + password)
		s(1)

# now that the password has been generated, randomly shuffle it (using
# 'shuffle' imported from the 'random' module at the start) ten times
# ('range(0,10)')
	for i in range(0,10):

# shuffle works on lists so convert the string 'password' to a list
		password = list(password)

# perform the shuffle
		shuffle(password)

# add the new list to an empty string and assign it to variable 'password'
		password = "".join(password)
		print("\n" + "Password shuffled to: " + password)
		s(1)

# finally, return the password
	return password

# with the 3 functions defined, run the programme by calling the first two
# functions in turn, assign the returned value to a variable and pass those two
# variables to the third function
userLength = getNumber()
pasComponents = getComponents()
yourPassword = makePass(userLength,pasComponents)

# print the password!
print("\n" + "Finally, here is your password: " + yourPassword)
