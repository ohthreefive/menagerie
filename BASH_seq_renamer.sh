#!/bin/sh

a=1

echo "\nfile extension?\n"
read ext
echo "\nfile prefix?\n"
read pref

for i in *$ext; do
	b=$(printf "%04d" "$a") #pads the numerical suffix to 4 characters
	c=$pref-$b$ext
	echo $i "->" $c
	mv $i $c
	a=$(($a+1)) #double brackets to perform maths
done

ls -l
