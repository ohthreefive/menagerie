#!/usr/bin/python3

# These three variables tell the getvalue() function for which value to ask

precontrast = 1
postcontrast = 2
delayed = 3

# These two variables tell the washout() function below which washouts to
# calculate

relative = 1
absolute_and_relative = 2

# This function gets a value for pre- or post-contrast or delayed from the
# user, looping where appropriate (eg. in case a letter is entered by mistake)

def getvalue(when):
	if when == 1:
		value1 = input("""Average Hounsfield units of the lesion pre-contrast:
			
(enter 'x' if unknown) """)
		return value1
	elif when == 2:
		while True:
			try:
				value2 = int(input("""
Average Hounsfield units of lesion post-contrast:
	
(ie. portal venous phase) """))
				return value2
				break
			except ValueError:
				print("\n" + "That's not a number. You'll need to start again.")
	elif when == 3:
		while True:
			try:
				value3 = int(input("""
Average Hounsfield units of the lesion after a delay:

(ie. ~10mins post-contrast) """))
				return value3
				break
			except ValueError:
				print("\n" + "That's not a number. You'll need to start again.")

# These two functions do simple calculations to return relative or absolute
# washout
# They are used by the washout() function where appropriate

def calcapw():
	abswashout = (((post - delay)/(post - int(pre)))*100)
	return abswashout

def calcrpw():
	relwashout = ((post - delay)/post*100)
	return relwashout

# The washout() function calls the above calculators where appropriate and
# tells the user whether the returned value classifies the lesion as benign or
# not

def washout(x):
	rpw = int(calcrpw())
	if x == 1:
		if rpw > 40:
			print("""
Relative percentage washout is {rpw}%.

This percentage of washout suggests the lesion is benign.""".format(rpw =
str(rpw)))
		else:
			print("""
Relative percentage washout is {rpw}%.

This percentage of washout suggests the lesion is NOT benign.""".format(rpw =
str(rpw)))
	elif x == 2:
		apw = int(calcapw())
		if apw > 60 and rpw >40:
			print("""
Relative percentage washout is {rpw}%
and absolute percentage washout {apw}%.

These percentage washouts suggests the lesion is benign.""".format(rpw =
str(rpw), apw = str(apw)))
		else:
			print("""
Relative percentage washout is {rpw}%
and absolute percentage washout is {apw}%.

These percentage washouts suggests the lesion is NOT benign.""".format(rpw =
str(rpw), apw = str(apw)))

# The body of the program iterates around the returned value for pre-contrast,
# on which the rest of the program depends

while True:
	try:
		pre = getvalue(precontrast)
		if pre == "x" or pre == "X":
			print("""
Absolute percentage washout cannot be calculated without pre-contrast value.

Only relative percentage washout will be calculated.""")
			post = getvalue(postcontrast)
			delay = getvalue(delayed)
			washout(relative)
			break
		elif int(pre) < 10:
			print("""
The lesion can be considered to be benign on the unenhanced value only.
Enhancement values are not required.""")
			break
		else:
			post = getvalue(postcontrast)
			delay = getvalue(delayed)
			washout(absolute_and_relative)
			break
	except ValueError:
		print("\n" + "Please only enter 'x' or a number." + "\n")
