# import functions from modules
from textwrap import wrap
from clipboard import set
from sys import argv
from webbrowser import open
from urllib import parse

# Drafts has passed the url to open the current draft and full content of
# current draft via share sheet
# These are accessed from the sys.argv list they populate and assigned to
# variables:
uuid = argv[1]
draft = argv[2]
chars = argv[3]

# textwrap works on lists, not strings
# convert string 'draft' to list, using new lines to split the string up
# assign variable name to the new list
inputList = draft.splitlines()

# an empty list which the loop below will fill:
outputList = []

# loop through list
for i in inputList:
# if item in list is blank (ie a blank line in the original draft)
# add 'NEWLINE' to outputList
# if i don't do this, these blank lines will be lost by the wrap() function
	if i == '':
		outputList.append('NEWLINE')
# otherwise use wrap() function to limit line width to 80 characters
# wrap outputs to a list which ive called shortened
	else:
		shortened = wrap(i, width = 80)
# iterate through the shortened list, adding each entry of that list to
# outputList
# if i just added 'shortened' to 'outputList', i would have lists within a list
# i want one list of strings
		for j in shortened:
			outputList.append(j)

# take the outputList and make it back in to a string, adding a new line
# between each line item
outputText = "\n".join(outputList)
# replace the NEWLINE placeholder with nothing, restoring the empty lines
outputText = outputText.replace("NEWLINE","")

# url encode the newly generated text
outputTextEncoded = parse.quote(outputText)

# create variable containing url for Drafts
replaceOriginalDraft = "drafts5://x-callback-url/replaceRange?uuid=" + uuid + "&text=" + outputTextEncoded + "&start=0&length=" + chars

# open the url. this will replace text in the original draft with the newly
# formatted text automagically.
open(replaceOriginalDraft)