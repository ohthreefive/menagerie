// See online documentation for examples
// http://getdrafts.com/scripting

/* simple drafts scrip action
take Draft & add to Things project
no other Things meta-data is added but this
would be easy to implement:
http://reference.getdrafts.com/objects/Things.html */

thing = editor.getText();
var todo = TJSTodo.create();
todo.title = thing;
todo.list = ["PROJECT NAME HERE!"];
var container = TJSContainer.create([todo]);
var cb = CallbackURL.create();
cb.baseURL = container.url;
var success = cb.open();
if (success) {
	console.log("Project created in Things");
}
else {
	context.fail();
}