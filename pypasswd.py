from random import choice as c, shuffle

def getNumber():
	
	while True:
		try:
			length = int(input("What length of password would you like?" + "\n"
			+ "(8-64 characters please)" + "\n" + "\n"))
			break
		except ValueError:
			print("\n" + "That's not a number. You'll need to start again." + "\n")
			
	if length < 8:
		print("\n" + "Too short, your password length has been increased to 8"
		+ "\n")
		length = 8
	elif length > 64:
		print("\n" + "Too long, your password length has been decreased to 64"
		+ "\n")
		length = 64
		
	return length

def getComponents():
	
	components = [0]
	
	while True:
		upper = input("\n" + "Include upper case/capital letters?" + "\n" + "(y/n)"
		+ "\n" + "\n").lower()
		if upper in ["y", "yes"]:
			components.append(1)
			break
		elif upper in ["n", "no"]:
			break

	while True:
		numerals = input("\n" + "Include numbers?" + "\n" + "(y/n)" + "\n"
		+ "\n").lower()
		if numerals in ["y", "yes"]:
			components.append(2)
			break
		elif numerals in ["n", "no"]:
			break

	while True:
		symbols = input("\n" + "Include symbols?" + "\n" + "(y/n)" + "\n"
		+ "\n").lower()
		if symbols in ["y", "yes"]:
			components.append(3)
			break
		elif symbols in ["n", "no"]:
			break

	return components

def makePass(userLength, pasComponents):
	password = ""
	chars = ["abcdefghijklmnopqrstuvwxyz", "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
	"0123456789", "-/:;()£&@.,?![]{}#%^*+=_\|~<>€$"]
	for i in pasComponents:
		password += c(chars[i])
	userLength -= len(pasComponents)
	for i in range(0,(userLength)):
		j = c(pasComponents)
		password += c(chars[j])
	for i in range(0,10):
		password = list(password)
		shuffle(password)
		password = "".join(password)
	return password

userLength = getNumber()
pasComponents = getComponents()
yourPassword = makePass(userLength,pasComponents)
print("\n" + yourPassword)
